const express = require('express');
var cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const sqlite = require('sqlite3').verbose();
const db = new sqlite.Database('./db/database.db');
const bcrypt = require('bcrypt');
const saltRounds = 12;
const jwt = require('jsonwebtoken');

app.use(bodyParser.json());
app.use(cors());

app.post('/login', function(req, rsp) {
    let query = 'select * from User where Name = ?';
    db.get(query, [req.body.username], (err, row) => {
        if(err) {
            rsp.status(500).json({error: err});
        } else {
            if(row) {
                bcrypt.compare(req.body.password, row.Password, function(error, result) {
                    if(result) {
                        var jwtToken = jwt.sign({
                            data: {
                                user: row.Name
                            }
                        }, 'webtech20', { expiresIn: 60 * 60 });
                        rsp.status(200).json({
                            msg: 'User logged in!', 
                            user: row.Id,
                            token: jwtToken
                        });
                    } else {
                        rsp.status(401).json({error: "Wrong password for " + req.body.username + "!"});
                    }
                });
            } else {
                bcrypt.hash(req.body.password, saltRounds, function(hashErr, hash) {
                    if(hashErr) {
                        rsp.status(500).json({error: hashErr});
                    } else {
                        db.run('INSERT INTO User(Name, Password) VALUES(?,?)', [req.body.username, hash], (err) => {
                            if(err) {
                                rsp.status(500).json({error: err});
                            } else {
                                db.get('select Id from User order by Id desc limit(1)', [], (err, row) => { 
                                    db.run('INSERT INTO Clicks(User) VALUES(?)', [row.Id], (err) => {
                                        if(err) {
                                            rsp.status(500).json({error: err});
                                        } else {
                                            var jwtToken = jwt.sign({
                                                data: {
                                                    user: row.Name
                                                }
                                            }, 'webtech20', { expiresIn: 60 * 60 });
                                            rsp.status(200).json({
                                                msg: 'User logged in!', 
                                                user: row.Id,
                                                token: jwtToken
                                            });
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            }
        }
    });
});

app.post('/users/:id/click', function(req, rsp) {
    jwt.verify(getToken(req.headers), 'webtech20', function(err, result) {
        if(err) {
            rsp.status(401).json({error: "Unauthorized!"});
        } else {
            let user = req.params.id;
            let query = 'select * from Clicks where User = ?';
            let updateQuery = 'update Clicks set Count = ? where User = ?';
            db.get(query, [user], (err, row) => {
                if(err) {
                    rsp.status(500).json({error: err});
                } else if(row) {
                    db.run(updateQuery, [row.Count + 1, user], (err2) => {
                        if(err) {
                            rsp.status(500).json({error: err2});
                        } else {
                            rsp.status(201).json(row.Count + 1);
                        }
                    });
                } else {
                    rsp.status(404).json({error: "User could not be found!"});
                }
            });
        }
    });
});

app.get('/users', function(req, rsp) {
    jwt.verify(getToken(req.headers), 'webtech20', function(err, result) {
        if(err) {
            rsp.status(401).json({error: "Unauthorized!"});
        } else {
            db.all('SELECT User.Id, Name, Count AS "Clicks" FROM User INNER JOIN Clicks ON User.Id = Clicks.User ORDER BY Count DESC', [], (err, rows) => {
                if(err) {
                    rsp.status(500).json({error: err});
                } else {
                    var r = [];

                    rows.forEach(element => {
                        r.push(element)
                    });

                    rsp.status(200).json(r);
                }
            });
        }
    });
});

function getToken(headers) {
    if (headers.authorization && headers.authorization.split(' ')[0] === 'Bearer') {
        return headers.authorization.split(' ')[1];
    } else {
        return null;
    }
}

app.listen(3000, () => {
    console.log("Server running on port 3000");
});
   