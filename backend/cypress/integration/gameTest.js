var token = "";

beforeEach(function fetchUser () {
    cy.request('POST', 'http://localhost:3000/login', {
        username: 'SecureAccount',
        password: 'test1234',
    }).then((res) => {
        expect(res.status).to.equal(200);
        expect(res.body.msg).to.equal('User logged in!');
        expect(res.body).to.have.any.keys('msg', 'token', 'user');
        expect(res.body.user).to.be.greaterThan(0);
        token = res.body.token;
    })
});

describe('Game Tests', function() {
	it('retrieves users', function() {
		cy.request({
			url: 'http://localhost:3000/users',
			auth: {
				bearer: token,
			},
		}).then(function(response) {
			expect(response.status).to.equal(200);
		});
    });

    it('click for a non-existing user, which will fail', function() {
        cy.request({
            method: 'POST',
            failOnStatusCode: false,
			url: 'http://localhost:3000/users/1500/click',
			auth: {
				bearer: token,
			},
		}).then(function(response) {
            expect(response.status).to.equal(404);
        });
    });

    it('click for a user', function(){
        var oldClicks = 0;
        cy.request({
            method: 'POST',
			url: 'http://localhost:3000/users/15/click',
			auth: {
				bearer: token,
			},
		}).then(function(response) {
            expect(response.status).to.equal(201);
            oldClicks = response.body;
        });
        
        cy.request({
            method: 'POST',
			url: 'http://localhost:3000/users/15/click',
			auth: {
				bearer: token,
			},
		}).then(function(response) {
            expect(response.status).to.equal(201);
            expect(response.body).to.be.greaterThan(oldClicks);
        });
    })
});