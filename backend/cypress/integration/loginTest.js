describe('Login Tests', function() {
	it('Login as a user', function() {
		cy.request({
			method: 'POST',
			url:'http://localhost:3000/login',
			failOnStatusCode: false,
			body: {
				username: 'SecureAccount',
				password: 'test1234',
			}
		}).then((res) => {
			expect(res.status).to.equal(200);
			expect(res.body.msg).to.equal('User logged in!');
			expect(res.body).to.have.any.keys('msg', 'token', 'user');
			expect(res.body.user).to.be.greaterThan(0);
		})
	});

	it('Login as a user with a wrong password gives a frobidden response', function() {
		cy.request({
			method: 'POST',
			url:'http://localhost:3000/login',
			failOnStatusCode: false,
			body: {
				username: 'SecureAccount',
				password: 'user',
			}
		}).then((res) => {
			expect(res.status).to.equal(401);
		})
	});
});

