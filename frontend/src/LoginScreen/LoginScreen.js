import React from 'react'

const loginscreen = (props) => {
    return(
        <div className="login-wrapper">
            <div>
                <from>
                    <span class="error">{props.loginError}</span>
                    <label>Name</label>
                    <span class="error">{props.nameError}</span>
                    <input type="text" placeholder="Username..." onChange={props.userNameChange}></input>
                    <label>Password</label>
                    <span class="error">{props.passwordError}</span>
                    <input type="password" placeholder="Password..." onChange={props.passwordChange}></input>
                    <div className="button-login" onClick={props.loginAction}>
                        <p>Login</p>
                    </div>
                </from>
            </div>
        </div>
    )
}

export default loginscreen;