import React from 'react';

const button = (props) => {
    return (
        <div className="button" onClick={props.whenClicked}>
            <p className="button-counter">{props.clicks}</p>
        </div>
    )
}

export default button;