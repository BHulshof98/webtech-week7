import React, { Component } from 'react';
import Person from '../Person/Person';
import { render } from 'react-dom';

const scoreboard = (props) => {
  var index = props.index;

  return (
    <div className="scoreboard-wrapper">
      <div className="scoreboard-body">
        {props.users.map(item => (
          <Person position={index++} current={props.currentUser} id={item.Id} name={item.Name} score={item.Clicks}></Person> 
        ))}
      </div>
    </div>
  )
}

export default scoreboard;