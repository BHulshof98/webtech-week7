import React from 'react';

const person = (props) => {
    return (
        <div className="scoreboard-item">
            <div className="item-cell"><div className="position">{props.position}</div></div>
            {props.current === props.id &&
                <div className="item-cell current"><b>{props.name}</b></div>
            }
            {props.current !== props.id &&
                <div className="item-cell"><p>{props.name}</p></div>
            }
            <div className="item-cell"><p>{props.score}</p></div>
        </div>
    )
}

export default person;