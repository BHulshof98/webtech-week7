import React, { Component } from 'react';
import './App.css';
import Button from './Button/Button';
import Person from './Person/Person';
import Scoreboard from './Scoreboard/Scoreboard';
import LoginScreen from './LoginScreen/LoginScreen';

class App extends Component {
  state = {
      currentUser: 0,
      username: "",
      password: "",
      users: [],
      loaded: false,
      userClicks: 0,
      token: "",
      loginError: "",
      passwordError: "",
      nameError: ""
  }

  componentDidMount() {
    if(this.state.currentUser === 0) {
      
    } else {
      this.getUsers();
    }
  };

  getInvalidFields() {
    var invalid = []
    if(this.state.username === "" || this.state.username == null)
      invalid.push("name");
    else
      invalid.push("");

    if(this.state.password === "" || this.state.password == null)
      invalid.push("password");
    else
      invalid.push("");

    if(invalid[0] === "" && invalid[1] == "")
      return null;
    else
      return invalid;
  }

  getUsers() {
      fetch("http://localhost:3000/users", {
        method: 'GET', 
        headers: {
          'Authorization': 'Bearer '+this.state.token
        }
      })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            users: result,
            loaded: true,
            userClicks: this.state.userClicks,
            currentUser: this.state.currentUser
          });
        },
        (error) => {
          this.setState({
              loaded: true,
            error
          });
        }
      )
  }

  /*
  * Login function for the login screen.
  * Could not make the login screen like this component.
  * React wouldn't let me do it so I did it like this.
  */
  login = (event) => {
    var invalidFields = this.getInvalidFields();
    console.log(invalidFields);
    if(!invalidFields) {
      fetch("http://localhost:3000/login", {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer '+this.state.token,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          username: this.state.username,
          password: this.state.password,
        })
      }).then(res => res.json())
      .then(
        (result) => {
          console.log("r",result);
          if(!result.error) {
            this.setState({
              users: [],
              loaded: false,
              userClicks: 0,
              currentUser: result.user,
              token: result.token,
              loginError: "",
              passwordError: "",
              nameError: ""
            })
            this.getUsers();
          } else {
            this.setState({
              loginError: result.error,
              passwordError: "",
              nameError: ""
            })
          }
        }
      )
    } else {
      var pwdErr = "";
      var nameErr = "";
      if(invalidFields[0] == 'name')
        nameErr = "Your name is missing!";
      if(invalidFields[1] == 'password')
        pwdErr = "Your password is missing!"
      this.setState({
        loginError: "",
        passwordError: pwdErr,
        nameError: nameErr
      })
    }
  }

  logout = (event) => {
    this.setState({
      users: [],
      loaded: false,
      userClicks: 0,
      currentUser: 0
    });
    alert("You have been logged out!")
  }

  userNameChange = (event) => {
    console.log(event.target.value);
    this.setState({
      username: event.target.value,
      password: this.state.password
    });
  }

  passwordChange = (event) => {
    this.setState({
      username: this.state.username,
      password: event.target.value
    });
  }

  incrementClicks = (event) => {
    var currentClicks = this.state.userClicks;
    fetch("http://localhost:3000/"+this.state.currentUser+"/click", {
        method: 'POST',
        headers: {
          'Authorization': 'Bearer '+this.state.token,
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      })
      .then(
        (result) => {
          this.getUsers();
          this.setState({userClicks: ++currentClicks})
        }
      )
  }

  render() {
      if(this.state.currentUser !== 0) {
        if (!this.state.loaded) {
            return <div>Loading...</div>;
        } else {
            var index = 1;
            return [
              <h1>Clicks this session:</h1>,
              <div class="logout-button" onClick={this.logout.bind(this)}><p>Logout</p></div>,
              <div class="clr"></div>,
              <p>Click to increase!</p>,
              <Button clicks={this.state.userClicks} whenClicked={this.incrementClicks.bind(this)} />,
              <h2>Scoreboard</h2>,
              <Scoreboard index={1} currentUser={this.state.currentUser} users={this.state.users}></Scoreboard>
            ]
        }
      } else {
        return <LoginScreen loginAction={this.login.bind(this)} 
                            passwordChange={this.passwordChange.bind(this)} 
                            userNameChange={this.userNameChange.bind(this)}
                            loginError={this.state.loginError}
                            nameError={this.state.nameError}
                            passwordError={this.state.passwordError}></LoginScreen>
      }
  }
}

export default App;
